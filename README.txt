Created by Francisco Guerra (francisco.guerra@ulpgc.es) for teaching.

This  eclipse project  duplicates  several directories  and  files  to 
facilitate its execution by means of two tools: GREENFOOT and ECLIPSE.

The main goal  of this duplication is to facilitate teaching using the 
advantages of each tool: the main advantage of GREENFOOT is simplicity 
of use,  and the main advantage  of ECLIPSE is to become familiar with 
Test Driven Development (TDD).

There are two run configurations: 
  - GreenfootScenarioMain (to run with ECLIPSE), and 
  - RunGreenfootFromEclipse (to run with GREENFOOT).

Eclipse does not know Greenfoot and therefore does not immediately detect 
the changes  Greenfoot makes.  So it’s  very important  to press  F5 when 
Greenfoot IDE ends and we go back to Eclipse IDE.


These are the contents of its directories and files:

0.- There is  a hidden ".git" folder with the initial version  of this 
    scenario.  EGit is an Eclipse Team provider for Git.  This Eclipse 
    project includes Git initialization, so we can recover the initial 
    scenario if some mistake is made  that we are not able to recover.
    Some operations available to manage the scenario versions are:

       - Commit:  When you want to make a commit,  select Commit and 
         window will popup which asks for files to commit.

       - Push to Upstream: When you have made a commit, you can Push 
         those  changes to the server with this command. 

       - Fetch from Upstream:  When you want to update  your project 
         with  the latest commits someone  else have made to the Git 
         repository, you have to use this command. 
         
       - Pull:  If you have fetched  some changes  from the Upstream,  
         you  have to Pull these changes to Eclipses working project. 
         
       - Show in History:  This one shows  you the history  and made 
         changes in the project on its lifetime.

    You can get around  with these commands  in the world of Git and 
    Eclipse,  if you get some special needs,  I’m sure you will find 
    answers pretty fast with some Googling.

1.- Directory  "config" has  configuration  and data that ECLIPSE copies 
    into directory "bin" to use them during the execution of the program.
    
    - Subdirectories "images" and "sounds" have a link to the original
      images and sounds directories available in the GREENFOOT project.
      
    - File "defaultImages" links files available at config/images with
      Java  files  defined  in "src"  (in this way, the given image is 
      associated  by default with the class constructor if no image is 
      explicitly associated with the class).
                
    - File "standalone.properties"  has  the configuration  needed to run 
      the project with ECLIPSE (WorldClassName is the current application 
      world and must be located in the default package):
        
          project.name=ProjectName
          main.class=WorldClassName
             
      The RunGreenfootFromEclipse configuration (to run the program with
      the GREENFOOT  environment)  updates  this  file  with the current
      world and its project name  (there is bug in the "project renaming
      refactor"  feature  of  Eclipse  that  is automatically fixed when
      RunGreenfootFromEclipse is executed).

      There are two optional properties  which allow locking controls or
      hiding controls:

          scenario.lock=false
          scenario.hideControls=false
          
      When hideControls is set to true  (ie. controls are hidden), the
      application begins immediately when it is run; when set to false
      two buttons are added to start and stop its execution.


2.- Directory "src"  contains Java sources for ECLIPSE. It has a package
    named  "default package"  containing  all  the Java sources that the
    GREENFOOT tool need to handle. This package must have a World class.

    Programmers  may add  packages and move sources to such packages but
    they must  update accordingly the corresponding line located in file
    "defaultImages". For example:

          When the class is located in the default package:
              class.ClassName.image=NombreFicheroDeLaImagen
              
          When the class is located in PackageName:
              class.PackageName.ClassName.image=NombreFicheroDeLaImagen

    They  must  also  add  the name  of the new package to the GREENFOOT
    configuration  file  "defaultImages"  which  references  the  images 
    associated by default with each Java class.


3.- Directory "test" contains actors tests and it can be empty.  Here you 
    have how to write several kinds of tests by junit5.  Notice that Java  
    sources that you see in comments  are assumed  to be available in our  
    project (in the src directory), and they are needed for these tests.
 

       import static org.junit.jupiter.api.Assertions.*;

       import org.junit.jupiter.api.BeforeEach;
       import org.junit.jupiter.api.Test;

       import greenfoot.World;
       import greenfoot.junitUtils.WorldCreator;
       import greenfoot.junitUtils.EventDispatch;
       import greenfoot.junitUtils.SoundPlayed;
       import greenfoot.junitUtils.Random;
       import greenfoot.junitUtils.Ask;

       public class ExampleActorTest {

           private World world;

           @BeforeEach
           void setUp() throws Exception {
               world = WorldCreator.getWorld(400, 300, 1);
           }

           /**
            * Example that checks the position in the world
            *
            *    public class ExampleActor extends greenfoot.Actor{
            *    }
            */
           @Test 
           void testNombreMetodo() {  // ???
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 4);
               assertEquals(1, exampleActor.getX());
               assertEquals(4, exampleActor.getY());
           }

           /**
            * Example that checks a new method in a derived world 
            *    
            *    public class ExampleWorld extends greenfoot.World {
            *        public ExampleWorld(){
            *            super(300,200,1);
            *        }
            *        public ExampleActor addObjectAt(int x, int y){
            *            ExampleActor exampleActor = new ExampleActor();
            *            this.addObject(exampleActor, x, y);
            *            return exampleActor;
            *        }
            *    }

            *    public class ExampleActor extends greenfoot.Actor{
            *    }
            */
           @Test 
           void testCreateObjetsInExampleWorld() {
               ExampleWorld exampleWorld
                 = (ExampleWorld)WorldCreator.getWorld(ExampleWorld.class);
               ExampleActor exampleActor = exampleWorld.addObjectAt(20, 30);
               assertEquals(20, exampleActor.getX());
               assertEquals(30, exampleActor.getY());
           }

           /**
            * Example that checks the world behavior of ExampleActor
            * when its act() method is invoked:
            * 
            *    public class ExampleActor extends greenfoot.Actor{
            *        public void act() 
            *        {
            *             move(6);
            *        }
            *    }
            */
           @Test
           void testAct() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               WorldCreator.runOnce(world);
              
               assertEquals(7, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }

           /**
            * Example that checks the world behavior of ExampleActor
            * when its act() method is invoked:
            * 
            *    public class ExampleActor extends greenfoot.Actor{
            *        public void act() 
            *        {
            *             move(Greenfoot.getRandomNumber(10));
            *        }
            *    }
            */
           @Test
           void testAct() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               Random.set(3);
               WorldCreator.runOnce(world);
              
               assertEquals(4, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }
           @Test
           void testActRandomWithCallerClassName() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               Random.set(6, exampleActor.getClass());        
               exampleActor.act();
               assertEquals(7, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }
           @Test
           void testRandomWithOthersGetRandomNumber() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               Random.set(6, exampleActor.getClass());  
               Greenfoot.getRandomNumber(10);
               exampleActor.act();
               assertEquals(7, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }

           /**
            * Example that checks the world behavior of ExampleActor
            * when the mouse is clicked or when the right key is
            * pressed in the keyboard.
            * 
            *    public class ExampleActor extends greenfoot.Actor{
            *        public void act() 
            *        {
            *             move(2);
            *             if (Greenfoot.isKeyDown("right")){
            *                 move(4);
            *             }
            *             if (Greenfoot.mouseClicked(this)){
            *                 move(6);
            *             }
            *        }
            *    }
            */
           @Test // There is not any event
           void testWithoutEvens() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               WorldCreator.runOnce(world);
              
               assertEquals(3, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }
           @Test // There is a keyboard event
           void testKeyboardTyped() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.keyTyped("right");
               WorldCreator.runOnce(world);
              
               assertEquals(7, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }
           @Test // There is a mouse event
           void testMouseClicked() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.mouseClicked(1,1);
               WorldCreator.runOnce(world);
              
               assertEquals(9, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }
           @Test // There is a mouse event with cell size > 1 
           public void testAct_rigthB() {
               World world = WorldCreator.getWorld(15,15,20);
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.mouseClicked(1*world.getCellSize(),
                                          1*world.getCellSize());
               WorldCreator.runOnce(world);

               assertEquals(9, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }
           @Test // There are mouse and keyboard events
           void testMouseClickedAndKeyboardTyped() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.mouseClicked(1,1);
               EventDispatch.keyTyped("right");
               WorldCreator.runOnce(world);
              
               assertEquals(13, exampleActor.getX());
               assertEquals(1, exampleActor.getY());
           }

           /**
            * Example that checks a behavior in the world
            * with keyboard input, applying to ExampleActor
            * object with next act behavior:
            * 
            *       public void act() 
            *       {
            *           if (Greenfoot.isKeyDown("g")){
            *               this.setLocation(10, 20);
            *           }
            *           if (!Greenfoot.isKeyDown("g")){
            *               this.setLocation(100, 20);
            *           }
            *        }
            */
           @Test 
           void testType_g() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.keyTyped("g");
               exampleActor.act();
               assertEquals(100, exampleActor.getX());
               assertEquals(20, exampleActor.getY());
           }
           @Test 
           void testPress_g() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.keyPressed("g");
               exampleActor.act();
               assertEquals(10, exampleActor.getX());
               assertEquals(20, exampleActor.getY());
           }
           @Test 
           void testRelease_g() {
               ExampleActor exampleActor = new ExampleActor();
               world.addObject(exampleActor, 1, 1);
               EventDispatch.keyPressed("g");
               EventDispatch.keyReleased("g");
               exampleActor.act();
               assertEquals(100, exampleActor.getX());
               assertEquals(20, exampleActor.getY());
           }

           /**
            * Example that checks a behavior in the world
            * with input string by ask method, applying to 
            * ExampleActor object with next act behavior:
            * 
            *     private String ask;
            *
            *     @Override
            *     public void act(){
            *         if (Greenfoot.mouseClicked(this)){
            *             ask = Greenfoot.ask("");
            *         }
            *     }
            *
            *     public String getAsk(){
            *         return ask;
            *     }
            */
            @Test 
            void testAskOne() {
                ExampleActorAsk exampleActorAsk = new ExampleActorAsk();
                world.addObject(exampleActorAsk, 1, 1);
                EventDispatch.mouseClicked(1,1);
                Ask.set("One");
                WorldCreator.runOnce(world);
               
                assertEquals("One", exampleActorAsk.getAsk());
            }
            @Test 
            void testAskTwo() {
                ExampleActorAsk exampleActorAsk = new ExampleActorAsk();
                world.addObject(exampleActorAsk, 1, 1);
                EventDispatch.mouseClicked(1,1);
                Ask.set("Two");
                WorldCreator.runOnce(world);
        
                assertEquals("Two", exampleActorAsk.getAsk());
            }

           /**
            * Example that checks the sound played
            *
            *    public class ExampleActor extends greenfoot.Actor{
            *        Greenfoot.playSound("au.wav");
            *    }
            */
            @Test 
            void testSoundPlayedAct() {
                ExampleActor exampleActor = new ExampleActor();
                world.addObject(exampleActor, 10, 10);
                exampleActor.act();
                assertEquals("au.wav", SoundPlayed.get());
            }
            @Test 
            void testSoundPlayedRunOnce() {
                world.addObject(new ExampleActor(), 10, 10);
                WorldCreator.runOnce(world);
                assertEquals("au.wav", SoundPlayed.get());
            }
       }


--------------------------------------------------------

    The scenario stickman is part of the book:

        Introduction to Programming with Greenfoot
        (Second edition)
        by Michael Kölling
        ISBN: 0134054296

--------------------------------------------------------
